package com.lecuong.shop.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "collar_stype")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CollarStype {

    //kieu co ao

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "collarStype")
    private List<ProductItem> productItems = new ArrayList<>();
}
