package com.lecuong.shop.mapper;

import com.lecuong.shop.entity.Role;
import com.lecuong.shop.entity.User;
import com.lecuong.shop.exception.InternalException;
import com.lecuong.shop.model.request.user.UserSaveRequest;
import com.lecuong.shop.model.response.role.RoleResponse;
import com.lecuong.shop.model.response.user.UserDetailResponse;
import com.lecuong.shop.model.response.user.UserResponse;
import com.lecuong.shop.repository.RoleRepository;
import com.lecuong.shop.utils.BeanUtils;
import com.lecuong.shop.utils.PasswordHasher;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Data
public class UserMapper {

    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    public User mapToEntity(UserSaveRequest saveRequest) {

        try {
            User user = new User();
            BeanUtils.refine(saveRequest, user, BeanUtils::copyNonNull);
            List<Role> roles = roleRepository.findAllByIdIn(saveRequest.getIds());
            String password = PasswordHasher.hash(saveRequest.getPassword());
            user.setPassword(password);
            user.setRoles(new HashSet<>(roles));
            return user;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            throw new InternalException("Hệ thống đang xử lý");
        }
    }

    public UserDetailResponse mapToDetailResponse(User user){
        UserDetailResponse detailResponse = new UserDetailResponse();
        BeanUtils.refine(user, detailResponse, BeanUtils::copyNonNull);
        List<RoleResponse> roleResponses = user.getRoles()
                .stream()
                .map(roleMapper::mapToResponse)
                .collect(Collectors.toList());

        detailResponse.setRoles(roleResponses);
        return detailResponse;
    }

    public UserResponse mapToResponse(User user){
        UserResponse userResponse = new UserResponse();
        BeanUtils.refine(user, userResponse, BeanUtils::copyNonNull);
        return userResponse;
    }
}
