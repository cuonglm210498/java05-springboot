package com.lecuong.shop.exception;

import com.lecuong.shop.model.response.StatusResponse;
import org.springframework.http.HttpStatus;

public interface Status {

    StatusResponse SUCCESS = new StatusResponse("SHOP-00", "SUCCESS", 200);

    String INTERNAL = "SHOP-INTERNAL";
    String VALIDATE = "SHOP-VALIDATE";
    String  OBJECT_NOT_FOUND = "SHOP-NOT-FOUND";
    String ACCESS_DENIED = "SHOP-ACCESS-DENIED";
}
