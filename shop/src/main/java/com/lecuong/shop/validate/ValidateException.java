package com.lecuong.shop.validate;

public class ValidateException extends RuntimeException{

    public ValidateException(String message) {
        super(message);
    }
}
