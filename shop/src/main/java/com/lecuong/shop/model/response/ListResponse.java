package com.lecuong.shop.model.response;

import lombok.Data;

import java.util.List;

@Data
public class ListResponse <T>{

    private StatusResponse status;
    private long totalItem;
    private List<T> data;

    private ListResponse(long totalItem, List<T> data){
        this.totalItem = totalItem;
        this.data = data;
    }

    public static <T> ListResponse<T> of(long totalItem, List<T> data) {
        return new ListResponse<>(totalItem, data);
    }
}
