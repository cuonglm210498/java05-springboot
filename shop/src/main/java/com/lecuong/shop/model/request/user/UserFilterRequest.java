package com.lecuong.shop.model.request.user;

import com.lecuong.shop.model.request.BaseFilter;
import lombok.Data;

@Data
public class UserFilterRequest extends BaseFilter {
    private String name;
    private String address;
}
