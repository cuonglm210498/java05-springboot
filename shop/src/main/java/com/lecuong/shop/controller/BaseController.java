package com.lecuong.shop.controller;

import com.lecuong.shop.exception.Status;
import com.lecuong.shop.model.response.ListResponse;
import com.lecuong.shop.model.response.ObjectResponse;
import lombok.Data;

@Data
public class BaseController {

    public ObjectResponse ok(){
        ObjectResponse objectResponse = new ObjectResponse();
        objectResponse.setStatus(Status.SUCCESS);
        return objectResponse;
    }

    public ObjectResponse ok(Object value) {
        ObjectResponse response = new ObjectResponse();
        response.setStatus(Status.SUCCESS);
        response.setData(value);
        return response;
    }

    public ListResponse ok(ListResponse listResponse){
        listResponse.setStatus(Status.SUCCESS);
        return listResponse;
    }
}
